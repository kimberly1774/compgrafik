﻿using DMS.Application;
using DMS.Geometry;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System;
using System.Drawing;
using System.Collections.Generic;
using DMS.OpenGL;
using System.Linq;

// goodies
// bad goodies beides einbauen
// fehler bei hinzufügen der goodies beheben


// punkte werden gezählt & während dem spielen angezeigt (+evtl zeit)
// punkte werden bei verlieren angezeigt
// bei verlieren: button: nochmal spielen oder beenden

    
// punkte werden bei verlieren angezeigt, dann sprung zu menü
// menü: spiel starten, spiel beenden
// lost true: wenn player verfehlt
// collision verbessern
// resizen
// highscore
// collision von doubleMe verbessern: ist jetzt kopierter Code --> vererbung?

namespace Example
{
    class Controller
    {
        private Texture background;
        private Texture obstacle;
        private Texture doubleMeTex;

        private Texture fasterTex;
        private Texture growTex;
        private Texture removeColumnTex;
        private Texture moreRowsTex;

        private bool initialized = false;
        private bool lost = false;
        private bool bugFix1 = false;

        private Ball ball1;
        private Ball helpBall;

        private Box2D player;
        private List<Box2D> hitable;
        private List<Box2D> goodieDoubleMe;

        private List<Box2D> goodieGrow;
        private List<Box2D> goodieFaster;
        private List<Box2D> goodieMoreRows;
        private List<Box2D> goodieRemoveColumn;


        private List<int> mond;
        private Box2D doubleMe;
        private Box2D theEnd;
        private Box2D grow;
        private Box2D faster;
        private Box2D moreRows;
        private Box2D removeColumn;


        private float countHitablei = 1;
        private float countHitablej = 20f;

        private int countDoubleMe = 5;

        private int countUp = 0;
        private int countUpMax = 3;
        public float hittablesPosX = -1;
        public float hittablesPosY = 0.9f;



        private void init()
        {
            background = TextureLoader.FromBitmap(Resourcen.schwarz);
            obstacle = TextureLoader.FromBitmap(Resourcen.grau);
            doubleMeTex = TextureLoader.FromBitmap(Resourcen.gruen);
            fasterTex = TextureLoader.FromBitmap(Resourcen.blau);
            growTex = TextureLoader.FromBitmap(Resourcen.pink);
            removeColumnTex = TextureLoader.FromBitmap(Resourcen.gelb);
            moreRowsTex = TextureLoader.FromBitmap(Resourcen.rot);


            ball1 = new Ball(0, -1, 0.05f, 0.05f);
            ball1.dir.Y = -0.03f;
            player = new Box2D(0.0f, -0.95f, 0.9f, 0.02f);
            hitable = new List<Box2D>();
            theEnd = new Box2D(-1, -1.05f, 2, 0.1f);

            goodieDoubleMe = new List<Box2D>();
            goodieGrow = new List<Box2D>();
            goodieFaster = new List<Box2D>();
            goodieMoreRows = new List<Box2D>();
            goodieRemoveColumn = new List<Box2D>();
            
            for (int i = 0; i < countHitablei; i++)
            {
                for (int j = 0; j < countHitablej; j++)
                {
                    float breitebox = (2 / countHitablej);
                    Box2D Targets = new Box2D(hittablesPosX + breitebox * j, hittablesPosY + breitebox * i, breitebox, breitebox);
                    hitable.Add(Targets);
                }
            }

            for (int i = 0; i < countDoubleMe; i++)
            {
                int Min = 1;
                int Max = hitable.Count;
                Random randNum = new Random();

                int[] spaceAssign = new int[countDoubleMe];

                for (int j = 0; j < spaceAssign.Length; j++)
                {
                    spaceAssign[j] = randNum.Next(Min, Max);
                }
                float a = hitable[spaceAssign[i]].X;
                float b = hitable[spaceAssign[i]].Y;


                grow = new Box2D(10.4f, 0f, 0.1f, 0.1f);
                faster = new Box2D(-10.8f, 0f, 0.1f, 0.1f);
                moreRows = new Box2D(-11f, 0f, 0.1f, 0.1f);
                removeColumn = new Box2D(-10.4f, 0f, 0.1f, 0.1f);

                Random rnd = new Random();
                int whichGoodie = rnd.Next(5, 5);

                if (whichGoodie == 1)
                {
                    doubleMe = new Box2D(a, b, 0.1f, 0.1f);
                    goodieDoubleMe.Add(doubleMe);
                }
                
                if (whichGoodie == 2)
                {
                    grow = new Box2D(a, b, 0.1f, 0.1f);
                    goodieGrow.Add(grow);
                }
                if (whichGoodie == 3)
                {
                    faster = new Box2D(a, b, 0.1f, 0.1f);
                    goodieFaster.Add(faster);
                }
                if (whichGoodie == 4)
                {
                    moreRows = new Box2D(a, b, 0.1f, 0.1f);
                    goodieMoreRows.Add(moreRows);
                }
                if (whichGoodie == 5)
                {
                    removeColumn = new Box2D(a, b, 0.1f, 0.1f);
                    goodieRemoveColumn.Add(removeColumn);
                }
                hitable.RemoveAt(spaceAssign[i]);

            }
            initialized = true;
        }

        
        private void changeDir()
        {
            ball1.canCollide = true;
        }
        

        private void Update(float updatePeriod)
        {
            if (lost == false)
            {
                if (initialized == false)
                {
                    init();
                }

                if (Keyboard.GetState()[Key.Left])
                {
                    player.X -= updatePeriod * 1.2f;
                }
                else if (Keyboard.GetState()[Key.Right])
                {
                    player.X += updatePeriod * 1.2f;
                }





                if (ball1.box.Intersects(player))
                {
                    float differ = (player.CenterX - ball1.box.CenterX);
                    float playerhalf = player.SizeX * 0.5f;
                    float anglestrength = 0.02f;
                    float ratio = differ / playerhalf;
                    float dx = ratio * anglestrength * (-1);
                    float dy = ball1.dir.Y * (-1);

                    ball1.dir.X = dx;
                    ball1.dir.Y = dy;
                    ball1.canCollide = true;

                    countUp += 1;
                    if (countUp == countUpMax)
                    {

                        foreach (Box2D aPart in hitable)
                        {
                            aPart.Y -= 0.1f;
                        }
                        foreach (Box2D aPart in goodieDoubleMe)
                        {
                            aPart.Y -= 0.1f;
                        }
                        foreach (Box2D aPart in goodieGrow)
                        {
                            aPart.Y -= 0.1f;
                        }
                        foreach (Box2D aPart in goodieFaster)
                        {
                            aPart.Y -= 0.1f;
                        }
                        foreach (Box2D aPart in goodieMoreRows)
                        {
                            aPart.Y -= 0.1f;
                        }
                        foreach (Box2D aPart in goodieRemoveColumn)
                        {
                            aPart.Y -= 0.1f;
                        }

                        int hitableCountPrev = hitable.Count;


                        for (int j = 0; j < countHitablej; j++)
                        {
                            float breitebox = (2 / countHitablej);
                            Box2D Targets = new Box2D(hittablesPosX + breitebox * j, hittablesPosY, breitebox, breitebox);
                            hitable.Add(Targets);
                            hitable.LastIndexOf(Targets);
                        }

                        mond = new List<int>();
                        
                        for (int l = hitableCountPrev; l < hitable.Count; l++)
                        {
                            mond.Add(l);
                        }

                            var rnd = new Random();
                            var randomNumbers = Enumerable.Range(1, 19).OrderBy(x => rnd.Next()).Take(5).ToList();
                            int c = mond[randomNumbers[0]];
                            
                            float a = hitable[c].X;
                            float b = hitable[c].Y;

                            Random rund = new Random();
                            int whichGoodie = rund.Next(5, 5);

                            if (whichGoodie == 1)
                            {
                                doubleMe = new Box2D(a, b, 0.1f, 0.1f);
                                goodieDoubleMe.Add(doubleMe);
                            }

                            if (whichGoodie == 2)
                            {
                                grow = new Box2D(a, b, 0.1f, 0.1f);
                                goodieGrow.Add(grow);
                            }
                            if (whichGoodie == 3)
                            {
                                faster = new Box2D(a, b, 0.1f, 0.1f);
                                goodieFaster.Add(faster);
                            }
                            if (whichGoodie == 4)
                            {
                                moreRows = new Box2D(a, b, 0.1f, 0.1f);
                                goodieMoreRows.Add(moreRows);
                            }
                            if (whichGoodie == 5)
                            {
                                removeColumn = new Box2D(a, b, 0.1f, 0.1f);
                                goodieRemoveColumn.Add(removeColumn);
                            }
                           
                            hitable.RemoveAt(c);
                        
                        mond.Clear();


                    }
                    if (countUp == 3)
                    {
                        countUp = 0;
                    }

                }
                if (helpBall != null) // falls Helpball existiert
                {
                    if (helpBall.box.Intersects(player))
                    {
                        helpBall.canCollide = false;
                        float differ = (player.CenterX - helpBall.box.CenterX);
                        float playerhalf = player.SizeX * 0.5f;
                        float anglestrength = 0.02f;

                        float ratio = differ / playerhalf;
                        helpBall.dir.X = ratio * anglestrength * (-1);
                        helpBall.dir.Y = helpBall.dir.Y * (-1);
                        helpBall.canCollide = true;
                    }
                }


                /*if (ball1.box.Intersects(doubleMe) && (helpBall == null)) // wenn helpball nicht existiert
                {
                    helpBall = new Ball(0, 0, 0.025f, 0.025f);

                    helpBall.dir.Y = 0.03f;
                }*/



                for (int i = 0; i < goodieDoubleMe.Count; i++)
                {
                    if (goodieDoubleMe[i].Intersects(ball1.box) && (ball1.canCollide == true))
                    {
                        ball1.dir.Y = ball1.dir.Y * (-1);
                        goodieDoubleMe.RemoveAt(i);
                        i--;
                        ball1.canCollide = false;
                        helpBall = new Ball(0, 0, 0.025f, 0.025f);
                        helpBall.dir.Y = 0.03f;
                    }
                }

                for (int i = 0; i < goodieFaster.Count; i++)
                {
                    if (goodieFaster[i].Intersects(ball1.box) && ball1.dir.Y == -0.03f)
                    {
                        faster.X = 100;
                        faster.Y = 100;
                        ball1.dir.Y = -0.08f;
                        bugFix1 = true;
                    }
                    if (ball1.box.Intersects(player) && bugFix1 == true)
                    {
                        ball1.dir.Y = -0.03f;
                        bugFix1 = false;
                    }
                }


                for (int i = 0; i < goodieMoreRows.Count; i++)
                {
                    if (goodieMoreRows[i].Intersects(ball1.box))
                    {
                        moreRows.X = 100;
                        moreRows.X = 100;

                        for (int k = 0; k < 3; k++)
                        {
                            foreach (Box2D aPart in hitable)
                            {
                                aPart.Y -= 0.1f;
                            }
                            for (int j = 0; j < countHitablej; j++)
                            {
                                Box2D Targets = new Box2D(-1 + 0.1f * j, 0.2f + 0.5f - 0.1f * 0.5f, 0.1f, 0.1f);
                                hitable.Add(Targets);
                            }
                        }
                    }
                }


                for (int i = 0; i < goodieGrow.Count; i++)
                {
                    if (goodieGrow[i].Intersects(ball1.box) && ball1.box.SizeX == 0.05f && ball1.box.SizeY == 0.05f)
                    {
                        grow.X = 100;
                        grow.Y = 100;
                        ball1.box.SizeX = 0.1f;
                        ball1.box.SizeY = 0.1f;
                    }
                }

                for (int i = 0; i < goodieRemoveColumn.Count; i++)
                {
                    if (goodieRemoveColumn[i].Intersects(ball1.box))
                    {
                        float uet = hitable.Count;

                        for (int m = 0; m < hitable.Count; m++)
                        {
                            if (hitable[m].Y == goodieRemoveColumn[i].Y)
                            {
                                hitable.RemoveAt(m);
                            }
                        }
                    }
                }


                    for (int i = 0; i < hitable.Count; i++)
                {
                    if (hitable[i].Intersects(theEnd))
                    {
                        lost = true;
                    }

                    if (hitable[i].Intersects(ball1.box) && (ball1.canCollide == true))
                    {
                        Box2D overlap = null;
                        overlap = new Box2D(0.0f, 0.0f, 0.0f, 0.0f);

                        if (hitable[i].Y < ball1.box.Y && hitable[i].X > ball1.box.X)
                        {
                            ball1.dir.X = ball1.dir.X * (-1);
                        }
                        else
                        {
                            ball1.dir.Y = ball1.dir.Y * (-1);
                        }

                        ball1.canCollide = false;
                        hitable.RemoveAt(i);
                        i--;
                    }
                }
                for (int i = 0; i < hitable.Count; i++)
                {
                    if (hitable[i].Intersects(ball1.box) == false && (ball1.canCollide == false))
                    {
                        changeDir();
                    }
                }

                if (helpBall != null) // wenn helpball existiert
                {
                    for (int i = 0; i < hitable.Count; i++)
                    {

                        if (hitable[i].Intersects(helpBall.box))
                        {
                            hitable.RemoveAt(i);
                            i--;
                            helpBall.dir.Y = helpBall.dir.Y * (-1);
                            helpBall.canCollide = false;
                        }
                    }
                }

                if ((ball1.box.MaxY < -1) | (ball1.box.MaxY > 1))
                {
                    ball1.dir.Y = ball1.dir.Y * -1;
                    ball1.canCollide = true;
                }
                if ((ball1.box.MaxX < -1) | (ball1.box.MaxX > 1))
                {
                    ball1.dir.X = ball1.dir.X * -1;
                    ball1.canCollide = true;
                }

                if (helpBall != null)
                {
                    if ((helpBall.box.MaxY > 1))
                    {
                        helpBall.dir.Y = helpBall.dir.Y * -1;
                        helpBall.canCollide = true;
                    }
                    if ((helpBall.box.MaxX < -1) | (helpBall.box.MaxX > 1))
                    {
                        helpBall.dir.X = helpBall.dir.X * -1;
                        helpBall.canCollide = true;
                    }
                    if (helpBall.box.MaxY < -1)
                    {
                        helpBall = null;
                    }
                }


                ball1.update();
                if (helpBall != null)
                {
                    helpBall.update();
                }
            }
        }


        private void Render()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.Color3(Color.White);
            GL.Enable(EnableCap.Blend);
            GL.Disable(EnableCap.Blend);
            //GL.Color3(Color.CornflowerBlue);
            DrawComplex(player);
            DrawComplex(ball1.box);


            if (lost == false)
            {
                DrawTexturedRect(new Box2D(-1, -1, 2, 2), background);

                if (helpBall != null)
                {
                    DrawComplex(helpBall.box);
                }

                DrawTexturedRect(theEnd, doubleMeTex);


                for (int i = 0; i < goodieDoubleMe.Count; i++)
                {
                    DrawTexturedRect(goodieDoubleMe[i], doubleMeTex);
                }

                for (int i = 0; i < goodieGrow.Count; i++)
                {
                    DrawTexturedRect(goodieGrow[i], growTex);
                }

                for (int i = 0; i < goodieFaster.Count; i++)
                {
                    DrawTexturedRect(goodieFaster[i], fasterTex);
                }

                for (int i = 0; i < goodieMoreRows.Count; i++)
                {
                    DrawTexturedRect(goodieMoreRows[i], moreRowsTex);
                }

                for (int i = 0; i < goodieRemoveColumn.Count; i++)
                {
                    DrawTexturedRect(goodieRemoveColumn[i], removeColumnTex);
                }

                for (int i = 0; i < hitable.Count; i++)
                {
                    DrawTexturedRect(hitable[i], obstacle);
                }

                DrawComplex(grow);
                DrawComplex(faster);
                DrawComplex(moreRows);
                DrawComplex(removeColumn);


                GL.LineWidth(2.0f);
                GL.Color3(Color.White);
                DrawBoxOutline(player);
                DrawBoxOutline(ball1.box);
                for (int i = 0; i < hitable.Count; i++)
                {
                    DrawBoxOutline(hitable[i]);
                }
            }

            if (lost == true)
            {
                DrawTexturedRect(new Box2D(-1, -1, 2, 2), doubleMeTex);
            }
        }

        private static void DrawTexturedRect(Box2D Rect, Texture tex)
        {
            tex.Activate();
            GL.Begin(PrimitiveType.Quads);
            //when using textures we have to set a texture coordinate for each vertex
            //by using the TexCoord command BEFORE the Vertex command
            GL.TexCoord2(0.0f, 0.0f); GL.Vertex2(Rect.X, Rect.Y);
            GL.TexCoord2(1.0f, 0.0f); GL.Vertex2(Rect.MaxX, Rect.Y);
            GL.TexCoord2(1.0f, 1.0f); GL.Vertex2(Rect.MaxX, Rect.MaxY);
            GL.TexCoord2(0.0f, 1.0f); GL.Vertex2(Rect.X, Rect.MaxY);
            GL.End();
            //the texture is disabled, so no other draw calls use this texture
            tex.Deactivate();
        }

        private void DrawBoxOutline(Box2D rect)
        {
            GL.Begin(PrimitiveType.LineLoop);
            GL.Vertex2(rect.X, rect.Y);
            GL.Vertex2(rect.MaxX, rect.Y);
            GL.Vertex2(rect.MaxX, rect.MaxY);
            GL.Vertex2(rect.X, rect.MaxY);
            GL.End();
        }

        private void DrawComplex(Box2D rect)
        {
            var xQuarter = rect.X + rect.SizeX * 0.25f;
            var x3Quarter = rect.X + rect.SizeX * 0.75f;
            var yThird = rect.Y + rect.SizeY * 0.33f;
            var y2Third = rect.Y + rect.SizeY * 0.66f;
            GL.Begin(PrimitiveType.Polygon);
            GL.Vertex2(rect.CenterX, rect.MaxY);
            GL.Vertex2(x3Quarter, y2Third);
            GL.Vertex2(rect.MaxX, rect.CenterY);
            GL.Vertex2(x3Quarter, yThird);
            GL.Vertex2(rect.MaxX, rect.Y);
            GL.Vertex2(rect.CenterX, yThird);
            GL.Vertex2(rect.X, rect.Y);
            GL.Vertex2(xQuarter, yThird);
            GL.Vertex2(rect.X, rect.CenterY);
            GL.Vertex2(xQuarter, y2Third);
            GL.End();
        }

        [STAThread]
        private static void Main()
        {
            var app = new ExampleApplication();
            var controller = new Controller();
            app.Render += controller.Render;
            app.Update += controller.Update;
            
            app.Run();
        }

    }
}